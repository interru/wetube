# encoding: utf-8

import gevent
from logbook import Logger
from redis import StrictRedis
from kombu import Consumer, Queue, connections, eventloop
from kombu.utils import nested

from ..broker import consumer_conn, exchange
from ..models import Playlist, Video
from ..videoinfo import loaders


class PlaylistWorker(gevent.Greenlet):

    def __init__(self):
        gevent.Greenlet.__init__(self)
        self.log = Logger('Playlist')
        self.db = StrictRedis()
        self.event = 'in.playlist'

    def _run(self):
        connection = connections[consumer_conn].acquire(block=True)
        channelpool = connection.ChannelPool()
        consumers = []
        channels = []


        for event in ('append', 'remove'):
            func = getattr(self, 'on_' + event)
            routing_key = '{}.{}'.format(self.event, event)

            channel = channelpool.acquire()
            queue = Queue(routing_key, exchange, routing_key)
            consumer = Consumer(channel, queue, callbacks=[func])

            channels.append(channel)
            consumers.append(consumer)

        with connection as conn:
            with nested(*(channels + consumers)):
                for _ in eventloop(conn, timeout=1, ignore_timeouts=True):
                    pass

    def on_append(self, body, message):
        print body
        data = body[0]
        for loader in loaders:
            if loader.check_url(data['url']):
                video = loader.load_video_info(data['url'])
                playlist = Playlist()
                playlist.append(video, after=data.get('after', None))
        message.ack()


    def on_remove(self, body, message):
        video = Video(body[0])
        if video.get('gid'):
            playlist = Playlist()
            playlist.remove(video)
        message.ack()
