# encoding: utf-8

import gevent
from json import dumps, loads
from logbook import Logger
from redis import StrictRedis
from kombu import Consumer, Queue, connections, eventloop

from ..broker import consumer_conn, out_exchange
from ..models import Playlist


class HistoryWorker(gevent.Greenlet):

    def __init__(self):
        gevent.Greenlet.__init__(self)
        self.db = StrictRedis()
        self.log = Logger("History")
        self.last = None

    def _run(self):
        playlist = Playlist()
        if playlist.playing:
            self.on_message(playlist.playing.dict())

        with connections[consumer_conn].acquire(block=True) as conn:
            queue = Queue('history_worker', out_exchange, 'out.playback')
            with Consumer(conn, queue, callbacks=[self.on_message]):
                for _ in eventloop(conn, ignore_timeouts=True):
                    pass

    def on_message(self, body, message=None):
        self.log.info("Add Video to History")
        self.db.lpush("history", dumps(body))
        if message:
            message.ack()
