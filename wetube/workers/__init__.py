# coding: utf-8

import gevent
from gevent import monkey; monkey.patch_all()
from gevent.pool import Pool

from history import HistoryWorker
from playstate import PlayStateWorker
from rotate import RotateWorker
from playlist import PlaylistWorker


def spawn():
    history = HistoryWorker()
    history.start()

    rotate = RotateWorker()
    rotate.start()

    state = PlayStateWorker(rotate)
    state.start()

    playlist = PlaylistWorker()
    playlist.start()
