# encoding: utf-8

import gevent
from time import sleep
from logbook import Logger
from redis import StrictRedis

from ..utils import wait_until
from ..broker import exchange, connections
from ..models import Playlist, Video


class RotateWorker(gevent.Greenlet):

    def __init__(self):
        gevent.Greenlet.__init__(self)
        self.db = StrictRedis()
        self.log = Logger("Rotate")
        self.schedule = None

    def _run(self):
        playlist = Playlist()
        video = playlist.playing
        if not video:
            video = Video(data=wait_until('on.video.added'))
        secs = video.remaining(secs=True)
        meth = self.schedule_rotate
        self.schedule = gevent.spawn_later(secs, meth, video)

    def schedule_rotate(self, video):
        while video.remaining():
            connected = int(self.db.get('connections'))
            if not connected:
                wait_until("out.connected")
            sleep(0.1)

        video = self.rotate()
        secs = video.remaining(secs=True)
        meth = self.schedule_rotate
        self.schedule = gevent.spawn_later(secs, meth, video)

    def skip(self):
        if self.schedule:
            self.schedule.kill()

        video = self.rotate()
        secs = video.remaining(secs=True)
        meth = self.schedule_rotate

        self.schedule = gevent.spawn_later(secs, meth, video)

    def rotate(self):
        playlist = Playlist()
        playlist.rotate()

        return playlist.playing

    def pause(self):
        if self.schedule:
            self.schedule.kill()
        playlist = Playlist()
        video = playlist.playing
        video.pause()

    def play(self):
        playlist = Playlist()
        video = playlist.playing
        if not (video and video['paused']):
            return
        video.play()

        secs = video.remaining(secs=True)
        meth = self.schedule_rotate
        self.schedule = gevent.spawn_later(secs, meth, video)
