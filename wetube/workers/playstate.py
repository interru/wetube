# encoding: utf-8

import gevent
from json import dumps, loads
from logbook import Logger
from redis import StrictRedis
from kombu import Consumer, Queue, connections, eventloop

from ..broker import consumer_conn, exchange


class PlayStateWorker(gevent.Greenlet):

    def __init__(self, rotate_worker):
        gevent.Greenlet.__init__(self)
        self.db = StrictRedis()
        self.log = Logger("PlayState")
        self.rotate_worker = rotate_worker

    def _run(self):
        with connections[consumer_conn].acquire(block=True) as conn:
            queue = Queue('in.playstate', exchange, 'in.playstate.*')
            with Consumer(conn, queue, callbacks=[self.on_message]):
                for _ in eventloop(conn, timeout=1, ignore_timeouts=True):
                    pass

    def on_message(self, body, message):
        if body[0] not in ('pause', 'play', 'skip'):
            return
        callback = getattr(self.rotate_worker, body[0])
        callback()
        message.ack()
