# coding: utf-8

import os
import gevent.local
from kombu import Exchange, Queue, Connection, connections, producers
from kombu.utils import nested

consumer_conn = Connection(os.environ.get('CLOUDAMQP') or 'amqp://')
producer_conn = Connection(os.environ.get('CLOUDAMQP') or 'amqp://')
exchange = Exchange('wetube', type='topic', delivery_mode=1)
out_exchange = Exchange('wetube.out', type='direct', delivery_mode=1)
local = gevent.local.local()
