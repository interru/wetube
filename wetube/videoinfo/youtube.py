# encoding: utf-8

from time import time
from urlparse import urlparse, parse_qs
from uuid import uuid4

import requests
from logbook import Logger
from redis import StrictRedis
from isodate import parse_duration

from ..models import Video, Playlist
from ..utils import compress_uuid


API_URL = "https://www.googleapis.com/youtube/v3/videos"
API_KEY = "AIzaSyCHyfH7Mq7Hn2FtHKZfAXWJFfGiC0n3j50"
PARTS = "snippet,contentDetails"

log = Logger('YoutubeInfo')


def check_url(url):
    url = urlparse(url)
    query = parse_qs(url.query)
    return url.hostname.endswith("youtube.com") and "v" in query


def load_video_info(url):
    url = urlparse(url)
    query = parse_qs(url.query)
    id = query["v"][0]

    params = {
        'id': id,
        'key': API_KEY,
        'part': PARTS
    }
    result = requests.get(API_URL, params=params)
    try:
        data = result.json()["items"][0]
    except KeyError:
        return

    duration = parse_duration(data["contentDetails"]["duration"])
    data = {
        'id': id,
        'type': 'youtube',
        'title': data["snippet"]["title"],
        'thumbnail': data["snippet"]["thumbnails"]["default"]["url"],
        'duration': int(duration.total_seconds() * 1000),
        'started': int(time() * 1000),
        'paused': False,
        'offset': 0
    }
    return Video(data=data, create=True)
