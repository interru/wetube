# coding: utf-8

from time import time
from uuid import uuid4
from json import loads, dumps

from redis import StrictRedis
from kombu import connections, producers

from ..broker import producer_conn, exchange
from ..utils import compress_uuid


class Video(object):
    """Video Model

    This object ist used to interact with the data of videos. Many
    methods in this object send messages to a broker to inform other
    processes that something changed. Please keep that in mind.

    :author: Mathias Koehler
    """

    def __init__(self, gid=None, data=None, create=False):
        """Initiates the video model

        :param str gid: The uuid of the video
        :param str data: Use initial data
        """
        gid = gid or data.get('gid', None) or compress_uuid(uuid4())
        self.db = StrictRedis()
        self.key = "video:{}".format(gid)
        self._data = {}

        data = data or {}
        if create:
            self['gid'] = gid
        for key, value in data.iteritems():
            if create:
                self[key] = value
            else:
                self._data[key] = value

    def start(self):
        """Starts the video

        This changes the start time to now and sets the pause state
        to None. Also he state change is send via the message broker
        """
        self['started'] = int(time() * 1000)
        self['paused'] = None
        self['offset'] = 0

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(self.dict(), 'out.playback')

    def play(self):
        """Resumes a video which is paused

        Saves an offset to the started time which represents the length
        of the pause without the current position. Use it in combination
        with ref:`started` to get the start point relative to the
        current time and without the pause.
        """
        now = int(time() * 1000)
        position_point = now - self.position
        offset = position_point - self['started']
        self['offset'] = offset
        self['paused'] = False

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(self.dict(), 'out.playstate.play')

    def pause(self):
        """Pauses the video.

        This saves a state in the redis database and also sends an
        message to the broker that the state has ben altered.
        """
        self['paused'] = int(time() * 1000)

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(self.dict(), 'out.playstate.pause')

    @property
    def position(self):
        """The position in msec

        This gets the msec since the video has started. Pauses are
        exluded with an offset. If the video is currently paused this
        returns the time when the pause state has started.
        """
        now = int(time() * 1000)
        if self['paused']:
            now = self['paused']

        offset = self.get('offset', 0)
        relative_start = self['started'] + offset
        return now - relative_start

    def remaining(self, secs=False):
        """The remaining time of the Video

        Returns the remaining time of the current video. If the time
        is ahead of the end it returns 0.
        """
        remaining = max(0, self['duration'] - self.position)
        if secs:
            return int(remaining // 1000)
        return remaining

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def dict(self):
        video = self.db.hgetall(self.key)
        for key, value in video.iteritems():
            video[key] = loads(value)
        return video

    def clear_cache(self):
        self._data = {}

    def __getitem__(self, key):
        if key not in self._data:
            item = self.db.hget(self.key, key)
            if not item:
                raise KeyError
            self._data[key] = loads(item)
        return self._data[key]

    def __setitem__(self, key, item):
        self.db.hset(self.key, key, dumps(item))
        self._data[key] = item
