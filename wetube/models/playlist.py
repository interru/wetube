# coding: utf-8

from redis import StrictRedis
from kombu import connections, producers

from ..broker import producer_conn, exchange
from .video import Video


class Playlist(object):
    """Playlist Model

    This is the representation of the current data. If you change
    something through this model the appropiate events will be fired
    which can and should be used to synchronsize the data with the
    Sockets and therefor with the Browsers.

    :author: Mathias Koehler
    """

    def __init__(self, name=None):
        """Initialize the Model

        :param str name: Optional name of the playlist (Default: None)
        """
        self.db = StrictRedis()
        self.key = "playlist"
        if name:
            self.key = "playlists:{}".format(name)

    def rotate(self):
        """Rotate the playlist

        This sends messages to the broker to inform what has changed.
        Messages that can potentionally be fired are:

            ['playlist.remove', 'playlist.append', 'playback']

        These are used to rotate the playlist.

        :returns: first (now playing) video
        """
        gid = self.db.lpop(self.key)
        self.db.rpush(self.key, gid)

        playing = self.playing
        playing.start()

        after = self[-2]
        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            if len(self) > 1:
                producer.publish(playing['gid'], 'out.playlist.remove')
            if after:
                data = {'after': after['gid'], 'video': Video(gid).dict()}
                producer.publish(data, 'out.playlist.append')

        return playing

    def append(self, video, after=None):
        """Appends a video

        This sends messages to the broker to inform what has changed.
        Messages that can potentionally be fired are:

            ['playlist', 'playlist.append', 'playback', 'video.added]

        These are used to rotate the playlist.
        """
        video = video.dict()
        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            if after:
                self.db.linsert(self.key, 'AFTER', after, video['gid'])
                data = {'after': after, 'video': video}
                producer.publish(data, 'out.playlist.append')
            else:
                self.db.rpush(self.key, video['gid'])
                if len(self) > 1:
                    playlist = map(lambda x: x.dict(), self)
                    producer.publish(playlist, 'out.playlist')
                else:
                    producer.publish(video, 'out.playback')
            producer.publish(video, 'on.video.added')

    def remove(self, video):
        video = video.dict()
        self.db.lrem(self.key, 0, video['gid'])
        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(video['gid'], 'out.playlist.remove')

    @property
    def playing(self):
        """First video (currently playing)"""
        return self[0]

    def __getitem__(self, index):
        if isinstance(index, slice):
            start = index.start or 0
            end = index.stop or -1
            gids = self.db.lrange(self.key, start, end)
            if not gids:
                return None
            return map(lambda x: Video(x), gids)
        gid = self.db.lindex(self.key, index)
        if not gid:
            return None
        return Video(gid)

    def __iter__(self):
        gids = self.db.lrange(self.key, 1, -1)
        for gid in gids:
            yield Video(gid)

    def __len__(self):
        return self.db.llen(self.key)
