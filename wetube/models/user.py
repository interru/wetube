# coding: utf-8

from simplejson import loads, dumps
from redis import StrictRedis


class User(object):

    def __init__(self, gid, data=None, create=False):
        self.db = StrictRedis()
        self.key = "users:{}".format(gid)
        self._data = {}

        data = data or {}
        if create:
            self['gid'] = gid
        for key, value in data.iteritems():
            if create:
                self[key] = value
            else:
                self._data[key] = value

    def dict(self):
        user = self.db.hgetall(self.key)
        for key, value in user.iteritems():
            user[key] = loads(value)
        return user

    def create(self):
        for key, value in self._data.iteritems():
            self[key] = value

    def clear_cache(self):
        self._data = {}

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def __getitem__(self, key):
        if key not in self._data:
            item = self.db.hget(self.key, key)
            if not item:
                raise KeyError
            self._data[key] = loads(item)
        return self._data[key]

    def __setitem__(self, key, item):
        self.db.hset(self.key, key, dumps(item))
        self._data[key] = item
