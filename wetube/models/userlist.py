# coding: utf-8

from redis import StrictRedis
from kombu import connections, producers
from simplejson import dumps, loads

from ..broker import producer_conn, exchange
from .user import User


class Userlist(object):
    """ Userlist Model

    Model for a Userlist. Currently we use more than one userlist. As
    an example we use an list for corrently connected users.
    """

    def __init__(self, name=None):
        self.db = StrictRedis()
        self.key = "users"
        if name:
            self.key = "users:{}".format(name)
        self.event = "out." + self.key.replace(':', '.')

    def add(self, user):
        if isinstance(user, str):
            user = User(user, data={'gid': user})
        self.db.sadd(self.key, user['gid'])

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(user.dict(), self.event + '.add')

    def remove(self, user):
        if isinstance(user, str):
            user = User(user, data={'gid': user})
        self.db.srem(self.key, user['gid'])

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(user.dict(), self.event + '.remove')

    def iternames(self):
        for user in self:
            username = user.get('name')
            if username:
                yield username

    def __iter__(self):
        gids = self.db.smembers(self.key)
        for gid in gids:
            yield User(gid)

    def __contains__(self, item):
        if isinstance(item, str) or isinstance(item, unicode):
            return self.db.sismember(self.key, item)
        return self.db.sismember(self.key, item.get('gid', None))

    def __len__(self):
        return self.db.scard(self.key)
