var server = require('tiny-lr')(),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    ngmin = require('gulp-ngmin'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch'),
    compass = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    path = require('path');


gulp.task('listen', function(next) {
  server.listen(35729, function(err) {
    if (err) return console.error(err);
    next();
  });
});


gulp.task('styles', ['listen'], function() {
  gulp.src('./sass/*.sass')
      .pipe(compass({
        config_file: './config.rb',
        css: './css',
        sass: './sass',
        project: __dirname
      }))
      .pipe(minifyCSS())
      .pipe(gulp.dest('../static/css'))
      .pipe(livereload(server));
});


gulp.task('javascript', function() {
  gulp.src('./js/**/*.js')
      .pipe(ngmin())
      .pipe(uglify())
      .pipe(concat('dist.js'))
      .pipe(gulp.dest('../static/js'))
});


gulp.task('watch', ['listen'], function() {
  gulp.watch('./js/**/*.js', ['javascript']);
  gulp.watch('./sass/*.sass', ['styles'])

});

gulp.task('default', ['watch']);
