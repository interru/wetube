angular.module("wetube.services", [
  "wetube.services.socket",
  "wetube.services.youtube",
  "wetube.services.sync_time"
]);

angular.module("wetube.directives", [
  "wetube.directives.player",
  "wetube.directives.ensureUnique",
  "wetube.directives.userlist",
  "wetube.directives.historylist",
  "wetube.directives.playlist",
  "wetube.directives.videoform",
  "wetube.directives.signup"
]);

angular.module("wetube.filters", [
  "wetube.filters.duration",
  "wetube.filters.reverse"
]);

angular.module("wetube.controllers", [
  "wetube.controllers.UserController",
  "wetube.controllers.ChatController",
  "wetube.controllers.PlaylistController",
  "wetube.controllers.VideoController",
  "wetube.controllers.HistoryController"
]);


angular.module('wetube', [
    "ngAnimate",
    "wetube.controllers",
    "wetube.services",
    "wetube.directives",
    "wetube.filters"
  ])
  .run(function(socket, sync_time) {
    sync_time.initiate()
  });

