angular.module("wetube.controllers.HistoryController", [
    "wetube.services.socket"
  ])
  .controller('HistoryController', function($scope, socket) {
    $scope.history = [];

    socket.on("history", function(videos) {
      $scope.history = videos
    });
    socket.on("playback", function(data) {
      if ($scope.current_video) {
        $scope.history.push(angular.copy($scope.current_video));
        if ($scope.history.length > 10) {
          $scope.history.splice(0, 1);
        }
      }
      $scope.current_video = data;
    });
  });
