angular.module("wetube.controllers.VideoController", [
    "wetube.services.socket",
    "wetube.services.youtube"  
  ])
  .controller('VideoController', function($rootScope, $scope, socket, youtube) {
    socket.on("playback", function(data, paused) {
      youtube.load(data.id, data.started - data.offset);
      if (data.paused) {
        youtube.pause();
        $scope.paused = true;
      } else {
        youtube.play(data.started + data.offset);
        $scope.paused = false;
      };
    });
    socket.on("playstate.pause", function() {
      youtube.pause();
      $scope.paused = true;
    });
    socket.on("playstate.play", function(video) {
      youtube.play(video.started + video.offset);
      $scope.paused = false;
    });

    $scope.changeState = function(state) {
      socket.emit('playstate.' + state, state);
    };
  });
