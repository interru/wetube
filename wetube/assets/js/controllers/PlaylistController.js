angular.module("wetube.controllers.PlaylistController", [
    "wetube.services.socket"
  ])
  .controller('PlaylistController', function($scope, socket) {
    $scope.playlist = [];
    $scope.history = [];

    socket.on("playlist", function(data) {
      $scope.showAddVideo = undefined;
      $scope.playlist = data;
    });

    socket.on("history", function(videos) {
      $scope.history = videos
    });
    socket.on("playback", function(data, paused) {
      $scope.current_video = data;
    });

    socket.on("playlist.remove", function(remove) {
      index = $scope.playlist.map(function(video, index) {
        if (video.gid === remove) {
          return index;
        }
      }).filter(isFinite)[0];
      $scope.playlist.splice(index, 1);
    });
    socket.on("playlist.append", function(data) {
      index = $scope.playlist.map(function(video, index) {
        if (video.gid === data.after) {
          return index;
        }
      }).filter(isFinite)[0] + 1;
      $scope.playlist.splice(index, 0, data.video);
    });

    $scope.input = {};
    $scope.addVideo = function(after) {
      var url = $scope.input.video;
      console.log(url);
      $scope.input.video = "";
      $scope.showAddVideo = undefined;
      socket.emit("playlist.append", {url: url, after: after});
    };
    $scope.removeVideo = function($event, gid) {
      console.log(gid);
      $event.stopPropagation();
      socket.emit("playlist.remove", gid);
    };

    $scope.toggleAddVideo = function(gid) {
      $scope.input.video = "";
      if ($scope.showAddVideo === gid) {
        $scope.showAddVideo = undefined;
      } else {
        $scope.showAddVideo = gid;
      }
    };
  });
