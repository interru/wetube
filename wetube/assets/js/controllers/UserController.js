angular.module("wetube.controllers.UserController", [
    "wetube.services.socket"  
  ])
  .controller('UserController', function($rootScope, $scope, socket) {
    $scope.login = function() {
      navigator.id.request();
    };
    $scope.logout = function() {
      navigator.id.logout();
    }
    navigator.id.watch({
      loggedInUser: null,
      onlogin: function(assertion) {
        socket.emit("signin", assertion);
      },
      onlogout: function() {
        socket.emit("logout");
        window.location.reload();
      }
    });
    socket.on("login_successful", function(user) {
      $scope.showSignUp = false;
      user.isMod = function() {
        return this.role === 'mod' || this.role === 'admin';
      }
      $rootScope.user = user;
      socket.emit("join");
    });

    $scope.users = [];
    socket.on('connected.users', function(data) {
      $scope.users = data;
    });
    socket.on('connected', function(data) {
      $scope.views = data;
    });
    socket.on('user.leaved', function(leaved_user) {
      $scope.users = $scope.users.filter(function(user) {
        return user.name !== leaved_user.name;
      });
    });
    socket.on('user.joined', function(user) {
      $scope.users.push(user);
    });

    $scope.input = {}
    $scope.dialog = false;
    $scope.submitSignUp = function() {
      socket.emit("signup", $scope.input.name);
      $scope.dialog = false;
    };
    $scope.cancelSignUp = function() {
      console.log('Jaaa');
      $scope.dialog = false;
      navigator.id.logout();
    };
    $scope.banUser = function(email) {
      socket.emit("ban", email);
    };
    var dialogToggle = function(name) {
      return function() {
        if ($scope.dialog !== name && $scope.dialog !== "signup") {
          $scope.dialog = name
        } else if ($scope.dialog === name) {
          $scope.dialog = false;
        }
        
      }
    };
    socket.on("signup_needed", function() {
      $scope.dialog = "signup";
    });
    $scope.toggleUserlist = dialogToggle('userlist');
    $scope.toggleHistory = dialogToggle('history');
    $scope.toggleAdmin = dialogToggle('admin');
  });
