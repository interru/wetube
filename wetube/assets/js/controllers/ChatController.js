angular.module("wetube.controllers.ChatController", [
    "wetube.services.socket"  
  ])
  .controller('ChatController', function($scope, socket, $log) {
    $scope.messages = [];
    $scope.input = {};

    $scope.submitAnon = function() {
      socket.emit("anon", $scope.input.name);
    };
    socket.on('messages', function(messages) {
      $scope.messages = messages;
    });
    socket.on('message', function(message) {
      $scope.messages.push(message);
    });
    $scope.addMessage = function() {
      var message = $scope.input.message;
      socket.emit("message", message);
      $scope.input.message = "";
    };
  });
