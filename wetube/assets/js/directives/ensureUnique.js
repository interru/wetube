angular.module('wetube.directives.ensureUnique', [
    "wetube.services.socket"
  ])
  .directive('ensureUnique', function(socket) {
    return {
      require: 'ngModel',
      link: function(scope, ele, attrs, c) {
        c.$parsers.unshift(function(value) {
          socket.emit("checkname", value);
          c.$setValidity('unique', true);
          return value;
        });
        socket.on("checkname", function(valid) {
          c.$setValidity('unique', valid);
        });
      }
    }
  });
