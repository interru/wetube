angular.module("wetube.directives.playlist", [])
  .directive('playlist', function(youtube) {
    return {
      restrict: 'AE',
      templateUrl: "static/templates/playlist.html",
      replace: true
    };
  });
