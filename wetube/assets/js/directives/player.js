angular.module("wetube.directives.player", [
    "wetube.services.youtube"
  ])
  .directive('player', function(youtube) {
    return {
      restrict: 'AE',
      link: function(scope, element) {
        youtube.bind(element[0]);
      }
    };
  });
