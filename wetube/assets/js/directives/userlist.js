angular.module("wetube.directives.userlist", [])
  .directive('userlist', function(youtube) {
    return {
      restrict: 'AE',
      templateUrl: "static/templates/userlist.html",
      replace: true
    };
  });
