angular.module("wetube.directives.videoform", [
    "wetube.services.socket"  
  ])
  .directive('videoform', function(youtube) {
    return {
      restrict: 'AE',
      templateUrl: "static/templates/videoform.html",
      replace: true
    };
  });
