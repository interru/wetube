angular.module("wetube.directives.signup", [])
  .directive('signup', function(youtube) {
    return {
      restrict: 'AE',
      templateUrl: "static/templates/signup.html",
      scope: {
        input: '=',
        submit: '&',
        cancel: '&'
      }
    };
  });
