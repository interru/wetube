angular.module("wetube.directives.historylist", [])
  .directive('historylist', function(youtube) {
    return {
      restrict: 'AE',
      templateUrl: "static/templates/historylist.html",
      scope: {
        history: '='
      },
      replace: true
    };
  });
