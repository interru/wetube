angular.module("wetube.filters.duration", [])
  .filter('duration', function() {
    return function(date) {
      if (angular.isNumber(Math.round(date / 1000))) {
        date = new Date(Math.abs(date));
      }
      return new Date(
          date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
          date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(),
          date.getUTCMilliseconds()
      );
    };
  });
