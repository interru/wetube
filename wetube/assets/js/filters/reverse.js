angular.module("wetube.filters.reverse", [])
  .filter('reverse', function() {
    return function(data) {
      if (!angular.isArray(data)) {
        return data;
      }
      return data.slice().reverse();
    };
  });
