angular.module("wetube.services.sync_time", [
    "wetube.services.socket"
  ])
  .service('sync_time', function ($rootScope, $interval, socket) {
    var round_trip, response_diff, request_time, time_diff_queue = [];

    this.time_diff = 0;
    this.latency = 0;
    $rootScope.time_diff = this.time_diff;
    $rootScope.latency = this.latency;

    socket.on('sync_time', function(request_diff, response_time) {
      now = (new Date()).getTime();
      response_diff = now - response_time;

      round_trip = (now - request_time);
      offset = ((response_time - request_time) + (response_time - now)) / 2;

      console.log(now + " - " + response_time);
      console.log(offset);

      time_diff_queue.push(offset);
      if (time_diff_queue.length > 100) {
        time_diff_queue.shift();
      }

      var index = Math.ceil(time_diff_queue.length / 2) - 1;
      this.time_diff = Math.floor(time_diff_queue.slice(0).sort(
        function (a, b) {
          return a - b;
        }
      )[index]);
      this.latency = round_trip;
      $rootScope.time_diff = this.time_diff;
      $rootScope.latency = this.latency;
    });

    $interval(function () {
      $rootScope.time = (new Date()).getTime() + $rootScope.time_diff;
      $rootScope.now = (new Date()).getTime();
    }, 300);

    this.initiate = function() {
      request_time = (new Date()).getTime();
      socket.emit('sync_time', request_time);
      $interval(function() {
        request_time = (new Date()).getTime();
        socket.emit('sync_time', request_time);
      }, 1000); 
    };
  });
