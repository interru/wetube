angular.module("wetube.services.youtube", [])
  .factory('youtube', function(
        $rootScope, $window, $timeout, $interval, $log, sync_time) {
    var service = $rootScope.$new(true);
  
    service.ready = false;
    service.create = false;
    service.paused = false;
    service.player = null;
    service.id = null;
    service.video = null;
    service.started = null;

    $window.onYouTubeIframeAPIReady = function () {
      $log.info('Youtube API is ready');
      service.ready = true;
    };

    $interval(function() {
      if (service.player && service.created) {
        if (service.paused) {
          service.player.pauseVideo();
        } else {
          var now = (new Date()).getTime();
          var position = ((now - service.started) / 1000)
          var position_diff = position - (service.player.getCurrentTime());
          if (position_diff > 2 || position_diff < -2) {
            service.player.seekTo(position);
          }
        }
      }
    }, 200);

    return {
      bind: function(element) {
        $log.info("Bind youtube to player element");
        element.id = "youtube_player";
        service.id = element.id;
      },
      load: function(video_id, started) {
        service.video = video_id;
        service.started = started;
        if (service.player && service.created) {
          var now = (new Date()).getTime();
          var time = (now - started) / 1000;
          service.player.loadVideoById(video_id, time);
        } else {
          this.create_player()
        }
      },
      create_player: function() {
        if (!service.ready) {
          $timeout(this.create_player, 200);
        }
        if (service.id && service.video) {
          $log.info('Create player!');
          if (service.player) {
            service.player.destroy();
          }
          service.player = new YT.Player(service.id, {
            videoId: service.video,
            width: "100%",
            height: "100%",
            playerVars: {
              modestbranding: 1
            },
            events: {
              'onReady': function() {
                service.created = true;
                var now = (new Date()).getTime();
                var start = (now - service.started) / 1000;
                service.player.seekTo(start, true);
              }
            }
          });
        }
      },
      player: function() {
        return service.player;
      },
      pause: function() {
        service.paused = true;
        service.player.pauseVideo();
      },
      isPaused: function() {
        return service.paused;
      },
      play: function(started) {
        service.started = started;
        if (service.player && service.created) {
          service.player.playVideo();
        }
        service.paused = false;
      }
    };
  });
