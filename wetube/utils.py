# -*- coding: utf-8 -*-

import string
import socket
from json import loads, dumps

import gevent
from kombu import Consumer, connections
from kombu.common import Broadcast

from .broker import consumer_conn, exchange


def create_event_handler(event):
    def event_handler(self, data=None, message=None):
        print "event " + event
        print data
        self.emit(event, data)
        if message:
            message.ack()
    return event_handler


class EventMeta(type):
    def __new__(cls, name, parents, dct):
        for event in dct.get('default_events', []):
            if not isinstance(event, str):
                routing_key = event[0]
                event = event[1]
                print "haa"
            else:
                routing_key = event

            handler = create_event_handler(event)
            event = 'send_' + routing_key.replace('.', '_')
            if event not in dct:
                print event
                dct[event] = handler
        return super(EventMeta, cls).__new__(cls, name, parents, dct)


class Event(object):
    __metaclass__ = EventMeta


def wait_until(event):
    outer = {'finished': False}
    def callback(body, message):
        outer['finished'] = True
        outer['body'] = body
        message.ack()
    with connections[consumer_conn].acquire(block=True) as conn:
        queue = Broadcast(event)
        with Consumer(conn, queue, callbacks=[callback]):
            while not outer['finished']:
                try:
                    conn.drain_events()
                    gevent.sleep(0) # Context switch
                except socket.timeout:
                    pass
    return outer['body']


def compress_uuid(uuid):
    characters = string.letters + string.digits
    uuid_int = getattr(uuid, 'int') or uuid

    output = []
    while uuid_int:
        uuid_int, digit = divmod(uuid_int, len(characters))
        output.append(characters[digit])
    return ''.join(output)


def decompress_uuid(uuid):
    characters = string.letters + string.digits
    length = len(characters)

    number = 0
    for char in reversed(uuid):
        number = number * length + characters.index(char)
    return UUID(int=number)


def extract(d, keys):
    return dict((k, d[k]) for k in keys if k in d)
