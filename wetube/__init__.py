# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, url_for
from socketio import socketio_manage
from .websocket import WetubeSocket
from kombu import Connection


app = Flask(__name__)
app.config.from_pyfile("config.cfg")


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/socket.io/<path:remaining>')
def socketio(remaining):
    try:
        socketio_manage(request.environ, {'/app': WetubeSocket})
    except:
        app.logger.error("Exception while handling socketio connection",
                         exc_info=True)
    return ""
