# coding: utf-8

from simplejson import dumps, loads
from kombu import producers

from ..utils import Event
from ..broker import producer_conn, exchange


class MessageMixin(Event):
    """Mixin for User specific events

    :author: Mathias Koehler
    """
    default_events = ['message']

    def send_messages(self, data=None):
        data = self.db.lrange("messages", 0,30)
        messages = map(lambda x: loads(x), data)
        messages.reverse()
        self.emit('messages', messages)

    def on_message(self, msg):
        data = {'user': self.user.dict(), 'msg': msg}
        self.db.lpush("messages", dumps(data))

        with producers[producer_conn].acquire(block=True) as producer:
            producer.exchange = exchange
            producer.publish(data, 'out.message')
