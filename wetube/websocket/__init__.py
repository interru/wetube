# coding: utf-8

import re
import math
import inspect
from uuid import uuid4
from time import time, sleep
from gevent import GreenletExit

from redis import StrictRedis
from socketio.namespace import BaseNamespace
from kombu import Producer, Consumer, Exchange, Queue,  eventloop, \
        connections, producers
from kombu.utils import nested

from ..broker import producer_conn, consumer_conn, exchange, out_exchange
from ..utils import compress_uuid
from ..models import Userlist
from .history import HistoryMixin
from .message import MessageMixin
from .playlist import PlaylistMixin
from .user import UserMixin



class WetubeSocket(PlaylistMixin, UserMixin, MessageMixin,
                   HistoryMixin, BaseNamespace):

    def __init__(self, *args, **kwargs):
        self.db = StrictRedis()
        super(WetubeSocket, self).__init__(*args, **kwargs)
        self.session["user"] = None

    def listener(self):
        """Listener for events from rabbitMQ

        This setups a listener for every event. On connect we dertemine
        which event we want to listen too. Then we spawn the the
        listener with gevent.
        """
        self.join_user()
        connection = connections[consumer_conn].acquire(block=True)
        channelpool = connection.ChannelPool()

        consumers = []
        channels = []
        initial_send = [
            'connected', 'users.connected', 'history', 'messages',
            'playlist', 'playback'
        ]

        channel = channelpool.acquire()
        bound_exchange = out_exchange(channel)
        bound_exchange.declare()
        bound_exchange.bind_to(exchange, routing_key='out.#')
        channel.release()

        for name, func in inspect.getmembers(self, inspect.ismethod):
            if not name.startswith('send_'):
                continue

            event_name = name[5:].replace('_', '.')
            routing_key = 'out.' + event_name
            queue_name = compress_uuid(uuid4())
            queue = Queue(queue_name, out_exchange, routing_key,
                          auto_delete=True, exclusive=True)

            channel = channelpool.acquire()
            channels.append(channel)
            consumers.append(Consumer(channel, queue, callbacks=[func]))

            if event_name in initial_send:
                func()

        try:
            with nested(connection, *(channels + consumers)) as conn:
                for _ in eventloop(conn[0], timeout=1, ignore_timeouts=True):
                    pass
        finally:
            self.leave_user()


    def publish_event(self, name, *args):
        """Publish an event to rabbitMQ

        This sends an event to rabbitMQ. Every event from Browser is
        proxied through to rabbitMQ if there isn't a method to handle
        that event. See :meth:`process_event`.
        """
        with producers[producer_conn].acquire(block=True) as producer:
            print "Ja mann eigentlich schon"
            name = "in." + name.replace('_', '.')
            print name
            producer.publish(args, name, exchange=exchange)

    def process_event(self, packet):
        """Process an event from the Browser.

        On default this event is proxied through to rabbitMQ. If there
        is a method with the name of the event. Every "." is replaced
        with an "_". Example:

            `"connected.users"` lookups and ivokes the method
            `on_connected_users(self, *args)` and if it isn't present
            then :meth:`publish_event(name, args)` is invoked.
        """
        args = packet['args']
        name = re.sub(r'[^a-zA-Z0-9]', "_", packet['name'])
        #print "browser: in_" + name
        #print args

        method = getattr(self, 'on_' + name, None)
        if not method:
            method = self.publish_event
            method(name, *args)
        else:
            method(*args)

    def get_initial_acl(self):
        return set(['recv_connect'])

    def recv_connect(self):
        """Send and setup everything the socket and Browser needs"""
        self.allowed_methods.update(
            ('on_signin', 'on_sync_time', 'recv_disconnect'))
        self.spawn(self.listener)

    def on_sync_time(self, client_time):
        server_time = int(math.ceil(time() * 1000))
        difference = server_time - client_time
        self.emit('sync_time', difference, server_time)

    def join_user(self):
        with producers[producer_conn].acquire(block=True) as producer:
            connected = self.db.incr("connections")
            producer.exchange = exchange
            producer.publish(connected, 'out.connected')

    def leave_user(self):
        with producers[producer_conn].acquire(block=True) as producer:
            connected = self.db.decr("connections")
            producer.exchange = exchange
            producer.publish(connected, 'out.connected')

        email = None
        if self.user:
            email = self.user.get('email', None)
        if email:
            map_user = lambda x: x[1].session['user']
            filter_user = lambda x: x.get('email', None) == email
            filter_none = lambda x: x is not None

            socks = self.socket.server.sockets.iteritems()
            users = filter(filter_none, map(map_user, socks))
            user_connections = filter(filter_user, users)

            if len(user_connections) < 1:
                connected = Userlist('connected')
                connected.remove(self.user)
