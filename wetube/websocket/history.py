# coding: utf-8

from simplejson import loads


class HistoryMixin(object):
    """Mixin for User specific events

    :author: Mathias Koehler
    """

    def send_history(self, data=None, message=None):
        videos = []
        items = self.db.lrange('history', 1, 10)

        for item in reversed(items):
            videos.append(loads(item))

        if videos:
            self.emit('history', videos)
