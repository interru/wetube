# coding: utf-8

import re
import requests

from ..utils import Event, compress_uuid
from ..models import User, Userlist


class UserMixin(Event):
    """Mixin for User specific events

    :author: Mathias Koehler
    """
    default_events = [('users.connected.add', 'user.joined'),
                      ('users.connected.remove', 'user.leaved')]

    @property
    def user(self):
        return self.session.get('user', None)

    @user.setter
    def user(self, item):
        self.session['user'] = item

    def send_connected(self, data=None, message=None):
        if not data:
            data = self.db.get("connections")
        self.emit('connected', data)
        if message:
            message.ack()

    def send_users_connected(self, data=None, message=None):
        """Send a list of connected users"""
        users = Userlist('connected')
        connected = []
        for user in users:
            if hasattr(user, 'dict'):
                connected.append(user.dict())
            else:
                connected.append(user)
        self.emit("connected.users", connected)
        if message:
            message.ack()

    def checkname(self, name):
        if not re.match(r"^[\w-]{3,15}$", name):
            return False
        users = Userlist()
        registered = name in users.iternames()
        connected = Userlist('connected')
        used = name in connected.iternames()
        return not registered and not used

    def on_checkname(self, name):
        """Checks if a username is free"""
        self.emit("checkname", self.checkname(name))

    def on_anon(self, name):
        if self.checkname(name):
            self.user = {
                'gid': compress_uuid(uuid4()),
                'name': name,
                'role': guest
            }

            self.emit("login_successful", self.user.dict())
            self.allowed_methods.update(('on_join', ))

    def on_signup(self, name):
        """Signup event

        Here the user can complete the signup procedure through choosing
        a free username which used for his user account.
        """

        if self.checkname(name):
            self.user.create()
            self.user['name'] = name
            users.add(self.user)

            self.emit("login_successful", self.user.dict())
            self.allowed_methods.update(('on_join', ))

    def on_signin(self, assertion):
        """Login event

        Socket.io event which is called for login and signup. If the
        assertion is correct we load the User Model. If the User isn't
        in redis we additionally append some data to the User Model.

        With a second step the :meth:`on_signup` the user can complete
        the signup procedure.
        """
        api_url = "https://verifier.login.persona.org/verify"
        params = {
            'assertion': assertion,
            'audience': "http://interru-wetube.herokuapp.com"
        }
        result = requests.post(api_url, params=params)

        data = result.json()
        users = Userlist()

        if data["status"] == "okay":
            gid = data['email']
            if gid not in users:
                data = dict(gid=gid, role='user', banned=False)
                self.user = User(gid, data)
            else:
                self.user = User(gid)

            if not self.user.get("name", None):
                self.emit("signup_needed")
                self.allowed_methods.update(('on_signup', 'on_checkname'))
            else:
                self.emit("login_successful", self.user.dict())
                self.allowed_methods.update(('on_join', ))
        else:
            self.emit("login_failed", data["reason"])

    def on_join(self, data):
        """Join event

        Inform other connected clients that a user has joined and
        assign the rights for using the chat or if the user is an admin
        or mod lift every restriction.
        """
        if self.allowed_methods:
            self.allowed_methods.update(('on_message', ))

        connected = Userlist('connected')
        if self.user not in connected:
            connected.add(self.user)

        if self.user.get('role', None) in ('mod', 'admin'):
            self.lift_acl_restrictions()
