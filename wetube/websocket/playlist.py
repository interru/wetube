# coding: utf-8

from ..utils import Event
from ..models import Playlist


class PlaylistMixin(Event):
    """Mixin for User specific events

    :author: Mathias Koehler
    """
    default_events = ['playlist.append', 'playlist.remove',
                      'playstate.pause', 'playstate.play', 'playstate.skip']

    def send_playlist(self, data=None, message=None):
        playlist = Playlist()
        videos = map(lambda x: x.dict(), playlist)
        if videos:
            self.emit('playlist', videos)
        if message:
            message.ack()

    def send_playback(self, data=None, message=None):
        playlist = Playlist()
        video = playlist.playing
        if not video:
            return
        self.emit('playback', video.dict())
        if message:
            message.ack()
