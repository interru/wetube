#!/usr/bin/env python
# coding: utf8

from socketio.server import SocketIOServer
from flask_script import Manager
from wetube import app
from wetube.workers import spawn
from redis import StrictRedis


manager = Manager(app)



@manager.command
def worker():
    spawn()


@manager.command
def clean():
    db.delete("playlist")


@manager.command
def runserver():
    db = StrictRedis()
    db.set("connections", 0)
    db.delete("connected")
    db.delete("users:connected")
    db.delete("history")
    SocketIOServer(('0.0.0.0', 5000), app,
        namespace="socket.io",  policy_server=False).serve_forever()


if __name__ == '__main__':
    manager.run()
